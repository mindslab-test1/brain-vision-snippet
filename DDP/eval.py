import os
import argparse
import numpy as np
from PIL import Image

import torch
import torch.nn as nn

import torchvision
from torchvision import transforms

from DDP import config

from model import Model

parser = argparse.ArgumentParser()
parser.add_argument("--input_path", type=str, required=True)
parser.add_argument("--output_path", type=str, required=True)

parser.add_argument("--checkpoint_dir", type=str, default=config.CHECKPOINT_DIR)
parser.add_argument("--continue_id", type=str, default=None)

parser.add_argument("--gpuNum", type=int, default=0)


args = parser.parse_args()


def load_model(Net, continue_id):
    filename = f'{config.MODEL_NAME}_{continue_id}.pth'
    checkpoint = torch.load(os.path.join(args.checkpoint_dir, filename), map_location='cpu')
    from collections import OrderedDict
    new_Net = OrderedDict()
    for k, v in checkpoint['Network'].items():
        name = k[7:] # remove `module.`
        new_Net[name] = v
    Net.load_state_dict(new_Net)

    return Net, checkpoint['Epoch']


device = torch.device(f"cuda:{args.gpuNum}" if torch.cuda.is_available() else "cpu")


Net = Model()
Net = Net.to(device)
Net.eval()


Net, epoch = load_model(Net, args.continue_id)
print(epoch)

img_transform = transforms.Compose([
    transforms.Resize((256, 256)),
    transforms.CenterCrop((256, 256)),
    transforms.ToTensor(),
])

input = Image.open(args.input_path)
input = input.convert('RGB')
input = img_transform(input)
input = input.unsqueeze(0)
with torch.no_grad():
    # region Feed Forward
    input = input.cuda()
    output = Net(input)
    # endregion

output = transforms.ToPILImage()(output[0].detach().cpu())
output.save(args.output_path)