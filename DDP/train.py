import os
import argparse

import torch
import torch.nn as nn
import torch.distributed as dist

from torch.optim import Adam
import torchvision
from torchvision import transforms
from torch.utils.data import DataLoader
from torch.utils.data.distributed import DistributedSampler

# region import modules
from model import Model
from dataset import Dataset
# endregion

import DDP.config as config
from DDP.distributed import DistributedDataParallel

from tensorboardX import SummaryWriter

parser = argparse.ArgumentParser()
parser.add_argument("--dataset_dir", type=str, default=config.DATASET_DIR)
parser.add_argument("--valset_dir", type=str, default=config.VALSET_DIR)
parser.add_argument("--checkpoint_dir", type=str, default=config.CHECKPOINT_DIR)

parser.add_argument("--gpuNum", type=int, default=0)
parser.add_argument("--logdir", type=str, default="log")
parser.add_argument("--continue_id", type=str, default=None)

parser.add_argument("--epoch", type=int, default=config.EPOCH)
parser.add_argument("--batch_size", type=int, default=config.BATCH_SIZE)
parser.add_argument("--learning_rate", type=float, default=config.LEARNING_RATE)

parser.add_argument("--n_gpus", type=int, default=config.NUM_GPUS)
parser.add_argument("--rank", type=int, default=0)
parser.add_argument("--dist_backend", type=str, default="nccl")
parser.add_argument("--dist_url", type=str, default=f"tcp://localhost:{config.DDP_PORT}")
parser.add_argument("--group_name", type=str, default='group_name')
args = parser.parse_args()


def load_model(Net, optimizer, continue_id):
    filename = f'{config.MODEL_NAME}_{continue_id}.pth'
    checkpoint = torch.load(os.path.join(args.checkpoint_dir, filename), map_location='cpu')
    Net.load_state_dict(checkpoint['Network'])
    optimizer.load_state_dict(checkpoint['Optimizer'])
    return Net, optimizer, checkpoint['Epoch'], checkpoint['Step']


def save_model(args, Net, optimizer, epoch, default_step):
    if not os.path.exists(args.checkpoint_dir):
        os.makedirs(args.checkpoint_dir, exist_ok=True)

    filename = f'{config.MODEL_NAME}_{args.logdir}_{epoch}.pth'
    dict1 = {
        'Network': Net.state_dict(),
        'Optimizer': optimizer.state_dict(),
        'Epoch': epoch,
        'Step': default_step,
    }
    torch.save(
        dict1,
        os.path.join(args.checkpoint_dir, filename)
    )

    print("checkpoint saved")


def init_distributed(args):
    assert torch.cuda.is_available(), "Distributed mode requires CUDA."
    print("Initializing distributed")
    # Initialize distributed communication
    torch.distributed.init_process_group(
        backend=args.dist_backend, init_method=args.dist_url,
        world_size=args.n_gpus, rank=args.rank, group_name=args.group_name)

    print("Done initializing distributed")


def prepare_dataloaders(args):
    img_transform = transforms.Compose([
        transforms.Resize((112, 112)),
        transforms.CenterCrop((112, 112)),
        transforms.ToTensor(),
    ])

    # region SetDataset
    train_dataset = Dataset(args.dataset_dir, transform=img_transform)
    val_dataset = Dataset(args.valset_dir, transform=img_transform)
    # endregion

    train_sampler = DistributedSampler(train_dataset)

    # region SetDataloader
    train_dataloader = DataLoader(train_dataset, num_workers=32, shuffle=False,
                                  sampler=train_sampler,
                                  batch_size=args.batch_size, pin_memory=False,
                                  drop_last=True, )
    val_dataloader = DataLoader(val_dataset, num_workers=32,
                                shuffle=False, batch_size=args.batch_size,
                                pin_memory=False, )
    # endregion

    return train_dataloader, val_dataloader


def model(args):
    # region SetDDPModel
    Net = Model(num_classes=args.num_classes).cuda()
    Net = DistributedDataParallel(Net)
    # endregion
    return Net


def validation(args, Net, Loss, val_dataloader):
    val_loss = 0

    Net = Net.eval()
    for ii, (input, label) in enumerate(val_dataloader):
        with torch.no_grad():
            # region FeedForward
            input = input.cuda()
            label = label.cuda()

            output = Net(input)
            loss = Loss(output, label)
            # endregion

            val_loss += loss.item()

    val_loss /= len(val_dataloader)

    Net = Net.train()

    return val_loss


def train(args):
    torch.cuda.set_device(args.gpuNum)
    init_distributed(args)

    torch.manual_seed(231231)
    torch.cuda.manual_seed(231231)

    Net = model(args)


    optimizer = Adam(
        params=Net.parameters(),
        lr=args.learning_rate,
    )

    Loss = nn.CrossEntropyLoss()

    train_dataloader, val_dataloader = prepare_dataloaders(args)
    print(f'one epoch is {len(train_dataloader)} iterations')

    default_epoch = default_step = 0
    if args.continue_id is not None:
        Net, optimizer, default_epoch, default_step = load_model(Net, optimizer, args.continue_id)

    if args.rank == 0:
        writer = SummaryWriter(os.path.join('log', args.logdir))

    for epoch in range(default_epoch, args.epoch):
        train_dataloader.sampler.set_epoch(epoch)
        tb_loss = 0
        for step, (input, label) in enumerate(train_dataloader):

            # region FeedForward
            input = input.cuda()
            label = label.cuda()

            output = Net(input)
            loss = Loss(output, label)
            # endregion

            # region BackProp
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            # endregion

            if args.rank == 0:
                # region Logging
                tb_loss += loss.item()
                if step % 1000 == 0 and step > 0:
                    iteration = default_step + len(train_dataloader) * (epoch - default_epoch) + step
                    save_model(args, Net, optimizer, epoch, iteration)

                    writer.add_scalar("Loss", tb_loss / 1000, iteration)

                    print(f'|{args.logdir}| epoch: {epoch}, step: {iteration}, loss: {tb_loss / 1000}')
                    tb_loss = 0
                # endregion

        if args.rank == 0:
            # region EndEpoch
            iteration = default_step + len(train_dataloader) * (epoch - default_epoch + 1)
            save_model(args, Net, optimizer, epoch, iteration)
            # endregion

def main():
    torch.backends.cudnn.benchmark = True
    args = parser.parse_args()

    train(args)


if __name__ == '__main__':
    main()
