# Distributed Data Parallel Snippet

### How to use
* 학습에 필요한 model과 dataset의 코드를 작성한다.
* `config.py`를 수정
* `train.py`의 코드 중 주석으로 region 표시가 되어있는 부분을 수정한다.

    예시)
    ```python
    # region FeedForward
    input = input.cuda()
    label = label.cuda()
    
    output = Net(input)
    loss = Loss(output, label)
    # endregion
    ```
* `wandb`, `learning rate scheduler`와 같이 필요한 코드들을 추가한다.
* `train0.sh`와 `train1.sh`를 참고하여 필요한 수 만큼의 script를 만들고 각각 실행한다. (n개의 모델을 동시에 띄울때 n개의 script가 필요)
* DDP로 학습한 모델을 load할 때는 기존 학습된 checkpoint들과 조금 다르게 key들 앞에 `module.`값이 붙는다. `eval.py`의 코드를 참고해서 사용.