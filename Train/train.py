import Train.config as config

import os
import argparse
import torch
import torch.nn as nn
from torch.optim import Adam
import torchvision
from torchvision import transforms
from torch.utils.data import DataLoader

from .model.model import Model
from .model.loss_ import Loss_
from .dataset.dataset import Dataset_

from tensorboardX import SummaryWriter

parser = argparse.ArgumentParser()
parser.add_argument("--dataset_dir", type=str, default=config.DATASET_DIR)
parser.add_argument("--valset_dir", type=str, default=config.VALSET_DIR)
parser.add_argument("--checkpoint_dir", type=str, default=config.CHECKPOINT_DIR)

parser.add_argument("--gpuNum", type=int, default=config.GPUNUM)
parser.add_argument("--logging_name", type=str, default=config.LOGGING_NAME)
parser.add_argument("--continue_id", type=str, default=None)

parser.add_argument("--epoch", type=int, default=config.EPOCH)
parser.add_argument("--batch_size", type=int, default=config.BATCH_SIZE)
parser.add_argument("--learning_rate", type=float, default=config.LEARNING_RATE)
args = parser.parse_args()


os.makedirs(os.path.join('log', args.logging_name), exist_ok=True)
writer = SummaryWriter(os.path.join('log', args.logging_name))
torch.backends.cudnn.benchmark = True
device = torch.device(f"cuda:{args.gpuNum}" if torch.cuda.is_available() else "cpu")


def load_model(Net, optimizer, continue_id):
    filename = f'{config.MODEL_NAME}_{continue_id}.pth'
    checkpoint = torch.load(os.path.join(args.checkpoint_dir, filename), map_location='cpu')
    Net.load_state_dict(checkpoint['Network'])
    optimizer.load_state_dict(checkpoint['Optimizer'])
    return Net, optimizer, checkpoint['Epoch'] + 1, checkpoint['Step']

def save_model(Net, optimizer, epoch, default_step):
    if not os.path.exists(args.checkpoint_dir):
        os.makedirs(args.checkpoint_dir)

    filename = f'{config.MODEL_NAME}_{args.logging_name}_{epoch}.pth'
    dict1 = {
        'Network': Net.state_dict(),
        'Optimizer': optimizer.state_dict(),
        'Epoch': epoch,
        'Step': default_step,
    }
    torch.save(
        dict1,
        os.path.join(args.checkpoint_dir, filename)
    )

    print("checkpoint saved")

@torch.no_grad()
def validation(Net, Loss, val_dataloader):
    val_loss = 0

    Net = Net.eval()
    for ii, (img, label) in enumerate(val_dataloader):
        # region Feed Forward
        img = img.to(device)
        label = label.to(device).long()

        output = Net(img, label)
        # endregion

        loss = Loss(output, label)

        val_loss += loss.item()

    val_loss /= len(val_dataloader)

    Net = Net.train()
    return val_loss




train_dataset = Dataset_(root=args.dataset_dir, transform=config.img_transform)
val_dataset = Dataset_(root=args.valset_dir, transform=config.img_transform)

train_dataloader = DataLoader(train_dataset, batch_size=args.batch_size, shuffle=True, num_workers=32)
val_dataloader = DataLoader(val_dataset, batch_size=512, shuffle=False, num_workers=10)

Net = Model()
Net = Net.to(device)
Net = Net.train()


Loss = Loss_()

optimizer = Adam(
    params=Net.parameters(),
    lr=args.learning_rate,
)

default_epoch = default_step = 0
if args.continue_id is not None:
    Net, optimizer, default_epoch, default_step = load_model(Net, optimizer, args.continue_id)

print(f"one epoch is {len(train_dataloader)} steps")



for epoch in range(default_epoch, args.epoch):
    tb_loss = 0

    for step, (img, label) in enumerate(train_dataloader):
        # region Feed Forward
        img = img.to(device)
        label = label.to(device).long()

        output = Net(img, label)
        # endregion

        # region Loss
        loss = Loss(output, label)
        # endregion

        # region Backprop
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        # endregion

        tb_loss += loss.item()

        if step % 1000 == 999:
            iteration = 1 + default_step + len(train_dataloader) * (epoch - default_epoch) + step
            save_model(Net, optimizer, epoch, iteration)
            writer.add_scalar("Loss", tb_loss / 1000, iteration)

            val_loss = validation(Net, Loss, val_dataloader)
            writer.add_scalar("Validation", val_loss, iteration)

            print(f'|{args.logging_name}| epoch: {epoch}, step: {iteration}, loss: {tb_loss / 1000}, val loss: {val_loss}')

            tb_loss = 0

    iteration = default_step + len(train_dataloader) * (1 + epoch - default_epoch)
    save_model(Net, optimizer, epoch, iteration)