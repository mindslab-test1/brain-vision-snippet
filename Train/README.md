# Train Snippet

### How to use
* `model/model.py`, `dataset.py`를 수정하여 학습할 Model과 Dataset을 작성
* `model/loss.py`에서 loss function 작성
* 모델의 output과 loss에 맞게 `train.py`
* `config.py`에서 경로와 hyperparameter들을 수정
* `requirements.txt` 수정
* `train.py`를 실행시켜 학습
    `python train.py`