import os
import random
from PIL import Image

import torch
from torch.utils.data import Dataset

class Dataset_(Dataset):
    def __init__(self, root, transform=None):
        super(Dataset_, self).__init__()
        self.root = root
        self.transform = transform
        self.length = 0

    def __getitem__(self, item):

        return torch.rand(1)

    def __len__(self):
        return self.length