MODEL_NAME = 'Model'

DATASET_DIR = ''
VALSET_DIR = ''
CHECKPOINT_DIR = ''
GPUNUM = 0
LOGGING_NAME = ''

EPOCH = 100

LEARNING_RATE = 1e-3
BATCH_SIZE = 32

from torchvision import transforms
img_transform = transforms.Compose([
    transforms.Resize((256, 256)),
    transforms.CenterCrop((256, 256)),
    transforms.ToTensor(),
])