import torch
import torch.nn as nn


class Loss_(nn.Module):
    def __init__(self):
        super(Loss_, self).__init__()
        self.cross_entropy = nn.CrossEntropyLoss()

    def forward(self, output, label):
        return self.cross_entropy(output, label)
