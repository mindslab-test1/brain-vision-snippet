import torch
import torch.nn as nn


class Model(nn.Module):
    def __init__(self):
        super(Model, self).__init__()
        self.model = nn.Sequential(
            nn.Linear(1, 1),
            nn.Tanh(),
        )

    def forward(self, x):
        return self.model(x)
