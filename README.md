# Brain Vision Snippet
Brain Vision 팀원들의 coding을 돕기위한 repository입니다. 코드를 재사용하기 쉽게 작성해주시고 설명을 간단히 남겨주세요.


### How to use
* Snippet을 task에 따라 폴더별로 분리하여 관리합니다.
* 각 폴더들은 독립적으로 관리됩니다.
* Branch를 만들어 수정할 때는 하나의 폴더에 대한 commit만 있어야합니다.