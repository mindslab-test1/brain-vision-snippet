# Loss Snippet

### How to use
* `train.py`에서 `from model.loss_ import [loss 이름]`로 loss 모듈 import
* task에 맞는 loss 클래스의 instance 생성 (ex. `Loss = PerceptualLoss_(device)`)

### Loss 목록
* CrossEntropyLoss (demo)
* PerceptualLoss
* PyramidPerceptualLoss

### Sample Code 실행 방법
* `dataset/dataset.py`에서 데이터셋을 선택(주석으로 선택)
* `train.py`에서 데이터와 호환되는 모델을 선택(ex. `Net = GenerationModel()`)
* task에 맞는 loss 클래스의 instance 생성 (ex. `GenerationModel`의 경우 모델의 생성물이 이미지이므로 `Loss = PerceptualLoss_(device)` 등을 사용)
* `python train.py`로 실행
