import torch
import torch.nn as nn

from vgg import VGG_Activations
from util import ImagePyramide
from torchvision.models import vgg19
from torch.nn import functional as F



class PerceptualLoss_(nn.Module):
    def __init__(self, device):
        super(PerceptualLoss_, self).__init__()
        self.device = device
        self.VGG19_AC = VGG_Activations(vgg19(pretrained=True).to(device), [1, 6, 11, 20, 29])

    def vgg(self, x):
        x = x.to(self.device)
        IMG_NET_MEAN = torch.Tensor([0.485, 0.456, 0.406]).reshape([1, 3, 1, 1]).to(self.device)
        IMG_NET_STD = torch.Tensor([0.229, 0.224, 0.225]).reshape([1, 3, 1, 1]).to(self.device)
        x = (x - IMG_NET_MEAN) / IMG_NET_STD
        return self.VGG19_AC(x)

    def forward(self, output, label):
        # VGG19 Loss
        vgg19_x_hat = self.vgg(label)
        vgg19_x = self.vgg(output)

        vgg19_loss = 0
        for i in range(0, len(vgg19_x)):
            vgg19_loss += F.l1_loss(vgg19_x_hat[i], vgg19_x[i])
        
        return vgg19_loss

