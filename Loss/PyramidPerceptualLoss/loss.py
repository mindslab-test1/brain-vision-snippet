import torch
import torch.nn as nn

from vgg import VGG_Activations
from util import ImagePyramide
from torchvision.models import vgg19
from torch.nn import functional as F



class PyramidPerceptualLoss_(nn.Module):
    def __init__(self, device):
        super(PyramidPerceptualLoss_, self).__init__()
        self.device = device

        self.scales = [1, 0.5, 0.25, 0.125]
        self.loss_weights = {
                'perceptual': [10, 10, 10, 10, 10]
        }
        self.pyramid = ImagePyramide(self.scales, 3, device)

        self.VGG19_AC = VGG_Activations(vgg19(pretrained=True).to(device), [1, 6, 11, 20, 29])
    
    def vgg(self, x):
        x = x.to(self.device)
        IMG_NET_MEAN = torch.Tensor([0.485, 0.456, 0.406]).reshape([1, 3, 1, 1]).to(self.device)
        IMG_NET_STD = torch.Tensor([0.229, 0.224, 0.225]).reshape([1, 3, 1, 1]).to(self.device)
        x = (x - IMG_NET_MEAN) / IMG_NET_STD
        return self.VGG19_AC(x)

    def forward(self, output, label):
        pyramide_real = self.pyramid(label)
        pyramide_generated = self.pyramid(output)

        value_total = 0
        for scale in self.scales:
            x_vgg = self.vgg(pyramide_generated['prediction_' + str(scale)])
            y_vgg = self.vgg(pyramide_real['prediction_' + str(scale)])
            for i, weight in enumerate(self.loss_weights['perceptual']):
                value = torch.abs(x_vgg[i] - y_vgg[i].detach()).mean()
                value_total += self.loss_weights['perceptual'][i] * value
        return value_total
