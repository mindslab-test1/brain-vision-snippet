import os
import math
import numpy as np
import transforms3d

def normalize_lm(lm):
    offset = (lm[0] + lm[16]) / 2

    bottom = lm[33].copy()
    for i in range(len(lm)):
        lm[i] -= bottom

    center = lm[27].copy()
    R_ = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]])
    if not (center[0] == 0 and center[1] == 0):
        cos_val = center[2] / math.sqrt(center[0] * center[0] + center[1] * center[1] + center[2] * center[2])
        z_angle = math.acos(cos_val if cos_val < 1 else 1)

        u_vector = np.array([center[1] / math.sqrt(center[0] * center[0] + center[1] * center[1]),
                             -center[0] / math.sqrt(center[0] * center[0] + center[1] * center[1]), 0])

        R = transforms3d.axangles.axangle2mat(u_vector, z_angle)
        R_ = transforms3d.axangles.axangle2mat(u_vector, -z_angle)
        for i in range(len(lm)):
            lm[i] = np.dot(R, lm[i])

    norm_val = 1
    if not lm[27][2] == 0:
        norm_val = lm[27][2].copy()
        for i in range(len(lm)):
            lm[i] = lm[i] / norm_val

    lm[27][0] = lm[27][1] = 0

    top = lm[39] + lm[42].copy()
    R__ = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]])
    if not (top[0] == 0 and top[1] == 0):
        cos_val = top[0] / math.sqrt(top[0] * top[0] + top[1] * top[1])
        angle = math.acos(cos_val if cos_val < 1 else 1)

        u_vector = np.array([0, 0, 1])

        R = transforms3d.axangles.axangle2mat(u_vector, angle)
        R__ = transforms3d.axangles.axangle2mat(u_vector, -angle)
        for i in range(len(lm)):
            lm[i] = np.dot(R, lm[i])

        # lm[27][1] = 0

    return lm.copy(), bottom.copy(), norm_val, R_, R__, offset


def denormalize_lm(lm, bottom, norm_val, R_, R__, offset):
    for i in range(len(lm)):
        lm[i] = bottom + norm_val * np.dot(R_, np.dot(R__, lm[i]))

    offset_ = (lm[0] + lm[16]) / 2
    diff = offset - offset_
    for i in range(len(lm)):
        lm[i] = lm[i] + diff

    return lm

def projection2d_lm(lm):
    lm_ = np.zeros((68,2))
    for i in range(len(lm)):
        lm_[i][0] = round(lm[i][0])
        lm_[i][1] = round(lm[i][1])

    return lm_
