# Face Domain Snippet

### How To Use
#### `3d_landmark_normalization.py`
Normalization & de-normalization for 3d landmark.

Input, output is `68x3` `numpy array`


#### `face_align.py`
Align face image based on Celeb-A HQ alignment

(https://arxiv.org/pdf/1710.10196.pdf, 14~15p)

Code was modified by FFHQ official repository (https://github.com/NVlabs/ffhq-dataset/blob/bb67086731d3bd70bc58ebee243880403726197a/download_ffhq.py#L259-L349)


#### `plot_landmark.py`
Plot facial landmark on white background
